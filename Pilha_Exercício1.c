#include <stdio.h>
#include <stdlib.h>

#define tam 20


//ESTRUTURA DA PILHA
typedef struct
{

	int item[tam];
	int topo;
	
}TPilha;

//FUNÇÃO PARA INICIALIZAR A PILHA
void pilha_Inicia(TPilha *p){
 
    p->topo = -1;

}

//FUNÇÃO PARA VERIFICAR PILHA VAZIA
int pilha_Vazia(TPilha *p){

	if(p->topo == -1){
		return 1;
	}else{
		return 0;
	}

}

//FUNÇÃO PARA VERIFICAR PILHA CHEIA
int pilha_Cheia(TPilha *p){
	
	if(p->topo == tam-1){
		return 1;
	}else{
		return 0;
	}
}

//FUNÇÃO PARA INSERIR ITEM NA PILHA
void pilha_Push(TPilha *p, int x){


    if(pilha_Cheia(p)==1){
    	printf("Erro: Pilha cheia!\n");
    }else{

        p->topo++;
        p->item[p->topo] = x;

    }

}

//FUNÇÃO PARA REMOVER ITEM DA PILHA
int pilha_Pop(TPilha *p){

    if(pilha_Vazia(p)==1){
       printf("Erro: Pilha cheia!\n");
    }else{
       int aux = p->item[p->topo];
       p->topo--;
       return aux;	
    }

}


int main(){

    
    TPilha *p; //CRIA A PILHA NO PROGRAMA PRINCIPAL
    int num,i,resto,resultado,aux; //DECLARAÇÃO DE VARIÁVEIS
    
    p = (TPilha *)malloc(sizeof(TPilha)); //ALOCAÇÃO DINÂMICA DA PILHA NA RAM

    pilha_Inicia(p); //INICIALIZA A PILHA

    //LEITURA DO NÚMERO A SER CONVERTIDO PARA BINÁRIO
    printf("Digite um valor: \n");
    scanf("%i",&num);

    
    //LAÇO PARA O PROCESSO DE DIVISÃO
    while(num>=2){

        resultado = num/2;  //ARMAZENA O RESULTADO DA DIVISÃO
        resto = num%2;     // ARMAZENA O RESTO DA DIVISÃO

       

        pilha_Push(p,resto); //EMPILHA O RESTO DA DIVISÃO NA PILHA
        
         //VERIFICA SE O RESULTADO É 1, OU SEJA, CHEGOU AO FINAL DO PROCESSO DE DIVISÃO (PARTE INTEIRA DO NÚMERO)
         if(resultado==1){
          pilha_Push(p,resultado); //EMPILHA O RESULTADO CASO SEJA O FINAL DO PROCESSO DE DIVISÃO
        }


        num=resultado; //NUM ARMAZENA O RESULTADO DA DIVISÃO E ENVIA NOVAMENTE PARA O LAÇO DE REPETIÇÃO

    }


    //IMPRIME O NÚMERO BINÁRIO OBTIDO
    printf("Número Binário: ");
    
    //LAÇO DE REPETIÇÃO PARA IMPRESSÃO DOS ITENS DA PILHA ENQUANTO ESTA NÃO FOR VAZIA
    while(pilha_Vazia(p)!=1){

        aux=pilha_Pop(p); //ARMAZENA O ITEM DESEMPILHADO NA VARIÁVEL AUX
        printf("%i",aux); //IMPRIME A VARIÁVEL AUX 

    }

     free(p); //LIBERA A PILHA DA RAM

}













