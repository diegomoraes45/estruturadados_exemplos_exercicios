#include <stdio.h>
#include <stdlib.h>


//CRIAR A ESTRUTURA DO NÓ DA LISTA
typedef struct no{

  int info;
  struct no *prox;

}No;

//Função para inicializar a Lista

No* inicializarLista(){

    return NULL;

}


//Verificar Lista Vazia
int verificaListaVazia(No* Lista){

    if(Lista==NULL){
    	return 1;
    }else{
    	return 0;
    }

}


//Função para alocar RAM
No* alocaRam(){

    No *no;
    no = (No*)malloc(sizeof(No));
    return no;
}

//Função inserir nó (elemento) na lista
No* insereNo(No* Lista, int valor){

   No *no = alocaRam();
   no->info =  valor;

   if(verificaListaVazia(Lista)==1){
   	  no->prox = NULL;
   	  Lista = no;
   }else{
   	  no->prox = Lista;
   	  Lista = no;
   }

   return Lista;

}

//Função imprime Lista
void imprimeLista(No* Lista){

   while(Lista!=NULL){

      printf("%i\n",Lista->info);
      Lista=Lista->prox;

   }

}


//Função Principal

int main(){

    
    No* lista = inicializarLista();
    lista=insereNo(lista,50);
    lista=insereNo(lista,30);

    imprimeLista(lista);

	return 0;
}








