#include <stdio.h>
#include <stdlib.h>

#define MAX 100

typedef struct{

   int pil[MAX];
   int topo;

}TPilha;


//Inicialização da Pilha
void pilha_Inicia(TPilha *p){

	p->topo = -1;

}

//Verifica Pilha Vazia
int pilha_Vazia(TPilha *p){
 
    if(p->topo == -1){
    	return 1;
    }else{
    	return 0;
    }

}

//Verifica Pilha Cheia
int pilha_Cheia(TPilha *p){

   if(p->topo == MAX-1){
   	    return 1;
   }else{
   	    return 0;
   }

}

//Inserir item na Pilha - PUSH
void pilha_Push(TPilha *p, int x){

    if(pilha_Cheia(p) == 1){
    	printf("Erro: Pilha Cheia\n");
    }else{
    	p->topo++;
    	p->pil[p->topo] = x;
    }

}

//Remover item da Pilha - POP
int pilha_Pop(TPilha *p){

    int aux;

    if(pilha_Vazia(p) == 1){
    	printf("Erro: Pilha Vazia\n");
    }else{
    	aux = p->pil[p->topo];
    	p->topo--;
    	return aux;
    }

}

int main()
{
	
    TPilha *p;

    p = (TPilha*)malloc(sizeof(TPilha));

    pilha_Inicia(p);
    pilha_Push(p,1);
    pilha_Push(p,2);
    pilha_Push(p,3);

    int aux;
    
    aux = pilha_Pop(p);

    printf("Saiu: %i\n",aux);
    
    aux = pilha_Pop(p);

    printf("Saiu: %i\n",aux);

    free(p);

	return 0;
}





