#include <stdio.h>
#include <stdlib.h>

#define MAX 100

//DEFINIÇÃO DA PILHA
typedef struct{

   int pil[MAX];
   int topo;

}TPilha;


//Inicialização da Pilha
void pilha_Inicia(TPilha *p){

  p->topo = -1;

}

//Verifica Pilha Vazia
int pilha_Vazia(TPilha *p){
 
    if(p->topo == -1){
      return 1;
    }else{
      return 0;
    }

}

//Verifica Pilha Cheia
int pilha_Cheia(TPilha *p){

   if(p->topo == MAX-1){
        return 1;
   }else{
        return 0;
   }

}

//Inserir item na Pilha - PUSH
void pilha_Push(TPilha *p, int x){

    if(pilha_Cheia(p) == 1){
      printf("Erro: Pilha Cheia\n");
    }else{
      p->topo++;
      p->pil[p->topo] = x;
    }

}

//Remover item da Pilha - POP
int pilha_Pop(TPilha *p){

    int aux;

    if(pilha_Vazia(p) == 1){
      printf("Erro: Pilha Vazia\n");
    }else{
      aux = p->pil[p->topo];
      p->topo--;
      return aux;
    }

}

//FUNÇÃO PARA IMPRIMIR A PÁGINA ATUAL
void imprime_Opcao(TPilha *p){
 
   int x = p->pil[p->topo];

  switch(x){

    
    case 1 : printf("Você está na página HOME\n"); break;
    case 2 : printf("Você está na página GAMES\n"); break;
    case 3 : printf("Você está na página TABLETS\n"); break;
    case 4 : printf("Você está na página CELULAR\n"); break;
    default: printf("Nenhuma página selecionada!\n"); break;


  }

}


int main(){

   int opt,controlador,navega, menu; //DECLARAÇÃO DE VARIÁVEIS

   TPilha *p; //CRIA A PILHA NO PROGRAMA PRINCIPAL

   p = (TPilha *)malloc(sizeof(TPilha)); //ALOCAÇÃO DINÂMICA DA PILHA NA RAM

   pilha_Inicia(p); //INICIALIZA A PILHA

   menu = 0; //MENU RECEBE ZERO PARA EXIBIR PELA PRIMEIRA VEZ O MENU PRINCIPAL

   //LAÇO DE REPETIÇÃO DO MENU PRINCIPAL
   do{
     
     if(menu==0){
        
     printf("Escolha uma opção:\n");
     printf("******************\n");
     printf("1-HOME\n");
     printf("2-GAMES\n");
     printf("3-TABLETS\n");
     printf("4-CELULAR\n");
     printf("******************\n");
     scanf("%i",&opt);

     //VERIFICA SE OPÇÃO DIGITADA É CORRETA
     if(opt>4||opt<=0){
         printf("Opção Inválida!\n");
         menu=0; //ATRIBUI VALOR PARA QUE SEJA EXIBIDO MENU PRINCIPAL NOVAMENTE
         controlador=1; //ATRIBUI VALOR 1 PARA MANTER CONDIÇÃO DE REPETIÇÃO
     }else{
         
     pilha_Push(p,opt); //EMPILHA VALOR DO ITEM ESCOLHIDO NO MENU

     imprime_Opcao(p); //IMPRIME A OPÇÃO ESCOLHIDA
     
      menu = 1; //ATRIBUI VALOR PARA EXIBIR O SUB-MENU
      
         
     }
     

     }
     
     //EXIBE SUB-MENU
     if(menu==1){
     
     //LEITURA DA OPÇÃO DO SUB-MENU
     printf("******************\n");
     printf("Escolha uma opção:\n");
     printf("******************\n");
     printf("1-IR PARA OUTRA PÁGINA\n");
     printf("2-VOLTAR PÁGINA\n");
     printf("3-SAIR\n");
     scanf("%i",&navega);
     
     
     //VERIFICA OPÇÃO ESCOLHIDA
     switch (navega){

       case 1 : controlador = 1; menu = 0; break; //CASO 1, RETORNA AO MENU PRINCIPAL
       case 2 : pilha_Pop(p); controlador = 1; menu = 1; imprime_Opcao(p); break; //CASO 2, REMOVE ITEM DO TOPO E IMPRIME
       case 3 : controlador = 0; menu = 3; break; //CASO 3, FINALIZAR O PROGRAMA
       default: printf("Opção Inválida\n");controlador = 1; menu = 1; imprime_Opcao(p); break; //VERIFICA OPÇÃO INVÁLIDA

       }
       

     }
     

     

   }while(controlador==1);
   


}














