#include <stdio.h>

int main()
{
	

    int vetor[3];
    int *ponteirogeral, *ponteiro1, *ponteiro2;
    int soma;

    for (int i = 1; i <= 3; ++i)
    {
    	printf("Digite o valor do item %i:\n",i);
    	scanf("%i",&vetor[i]);
    	
    }

    for (int i = 1; i <= 3; ++i)
    {
    	ponteirogeral = &vetor[i];
    	printf("ITEM %i --> Endereço: %p | Valor: %i",i,ponteirogeral,*ponteirogeral);
    }

    ponteiro1 = &vetor[1];
    ponteiro2 = &vetor[2];

    soma = *ponteiro1+*ponteiro2;

    printf("A soma dos valores é %i\n",soma);



	return 0;
}