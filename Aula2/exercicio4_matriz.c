#include <stdio.h>

int main(int argc, char const *argv[])
{
	//DECLARAÇÃO DE VARIÁVEIS
	int matriz[2][2], linha = 0, coluna = 0;
 
    //LAÇO PARA LEITURA DA MATRIZ
	for(linha = 0; linha < 2; linha++){
 
		for (coluna = 0; coluna < 2; coluna++){
            printf("Digite o valor L%d C%d: ",linha,coluna);
			scanf("%d",&matriz[linha][coluna]);
		}
 
	}
 
    //LAÇO PARA IMPRESSÃO DA MATRIZ
    for(linha = 0; linha < 2; linha++){
 
		for (coluna = 0; coluna < 2; coluna++){
            
            printf("%d ",matriz[linha][coluna]);
			
		}

		printf("\n");
 
	}
    
        printf("Diagonal Invertida da Matriz:\n");
 
    //LAÇO PARA IMPRESSÃO DA DIAGONAL INVERTIDA DA MATRIZ
	for (linha = 1; linha >= 0; linha--){
 
		for(coluna = 1; coluna >= 0; coluna--){
			if ( linha == coluna){
					printf("%d\n",matriz[linha][coluna]);
			}
		}
	}	
 
	
	return 0;
}