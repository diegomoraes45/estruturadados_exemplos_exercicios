#include <stdio.h>

#define linha 3
#define coluna 3

int main(int argc, char const *argv[])
{
	int mat[linha][coluna];
	
    //Preenchimento da Matriz
    for (int i = 0; i < linha; ++i)
    {
        for (int j = 0; j < coluna; ++j)
        {
            printf("Insira o valor da célula %d %d ",i,j);
            scanf("%d",&mat[i][j]);
        }
    }

    //Impressão da Matriz
    for (int i = 0; i < linha; ++i)
    {
        for (int j = 0; j < coluna; ++j)
        {
            printf("%d ",mat[i][j]);
        }
        //Adiciona espaço entre linhas
        printf("\n");
    }
    

	return 0;
}


