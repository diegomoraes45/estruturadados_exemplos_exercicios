#include <stdio.h>

int main(int argc, char const *argv[])
{


	int vet[3]; //DECLARAÇÃO DO VETOR
	int valor; //DECLARAÇÃO DA VARIÁVEL QUE RECEBERÁ O VALOR
    int i = 0; //DECLARAÇÃO E INICIALIZAÇÃO DO CONTADOR PRINCIPAL
    int verifica; //VARIÁVEL DE VERIFICAÇÃO DE VALOR EXISTENTE


    //LAÇO PRINCIPAL
    while(i<3)
     {
     	printf("Digite o valor %d: ",i);
     	scanf("%d",&valor);
     	verifica = 0;
        
        //VERIFICA SE É O PRIMEIRO VALOR DO VETOR
        if(i==0){
        	vet[i]=valor;
        	i++;
        }else{

        	//LAÇO DE VERIFICAÇÃO DE VALOR EXISTENTE NO VETOR
        	for (int j = 0; (j < i)&&(verifica!=1); j++)
        	{
        		if(vet[j]==valor){
        			printf("O valor digitado já existe! Digite outro valor.\n");
        			verifica = 1;
        		}else{
        		    verifica = 0;
        		}
        	}

             //INSERE NOVO VALOR SE NÃO EXISTIR         	
        	 if(verifica!=1){
        	    vet[i]=valor;
        	    i++;
        	}
        	
        }
        
       

     }

     //IMPRESSÃO DOS VALORES DO VETOR
     for (int i = 0; i < 3; i++)
      {
      	printf("%d\n",vet[i]);
      } 

	return 0;
}