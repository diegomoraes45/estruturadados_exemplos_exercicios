#include <stdio.h>
#include <stdlib.h>

//Estrutura do Nó da Lista
typedef struct no{

	int info;
	struct no *prox;


}No;

//Função para inicializar a Lista

No* inicializarLista(){

	return NULL;

}

//Função para alocar RAM
No* alocarRAM(){

	No *no = (No*)malloc(sizeof(No));
	return no;

}


//Função para verificar lista vazia
int listaVazia(No *Lista){

	if(Lista==NULL){

		return 1;

	}else{

		return 0;

	}

}


//Função para inserir no inicio da lista
No* inserirInicio(No *Lista, int x){

	No *no = alocarRAM();
	no->info = x;

	if(listaVazia(Lista)==1){

		no->prox = NULL;
		Lista = no;

	}else{

		no->prox = Lista;
		Lista = no;

	}
	
	return Lista;

}

//Função para inserir no meio da lista
No* inserirMeio(No* Lista, int x){

    No *no = alocarRAM();
    no->info = x;

    No *ant = NULL;
    No *ptr = Lista;

    while(ptr!=NULL && ptr->info<x){

        ant = ptr;
        ptr=ptr->prox;

    }

    if(ant==NULL){
       
       no->prox = Lista;
       Lista = no;

    }else{

    	no->prox = ant->prox;
    	ant->prox = no;
    }

     return Lista;
}



//Função para inserir no fim da lista
No* inserirFim(No *Lista, int x){

     No *no = alocarRAM();
     no->info = x;

     if(listaVazia(Lista)==1){
         
         no->prox = NULL;
         Lista = no;

     }else{
        
        No *aux = Lista;

        while(aux->prox!=NULL){

             aux = aux->prox;

        }

        no->prox = NULL;
        aux->prox = no;

     }

     return Lista;

}


//Imprime a Lista

void imprimeLista(No *Lista){

	No *aux = Lista;

	while(aux!=NULL){

		printf("%i\n",aux->info);
		aux=aux->prox;

	}

}





//Programa principal
int main(){

	No *lista = inicializarLista();
    lista = inserirInicio(lista,80);
	lista = inserirInicio(lista,75);
	lista = inserirInicio(lista,30);
	
	
	
	
	imprimeLista(lista);

	return 0;
}







