#include <stdio.h>
#include <stdlib.h>

#define MAX 100

typedef struct{

   int fila[MAX];
   int tamanho;

}TFila;


//Inicialização da Fila
void fila_Inicia(TFila *f){

    f->tamanho = -1;

}

//Verifica Fila Vazia
int fila_Vazia(TFila *f){
 
    if(f->tamanho == -1){
        return 1;
    }else{
        return 0;
    }

}

//Verifica Fila Cheia
int fila_Cheia(TFila *f){

   if(f->tamanho == MAX-1){
        return 1;
   }else{
        return 0;
   }

}

//Enfileirar item
void fila_Insere(TFila *f, int x){

    if(fila_Cheia(f) == 1){
        printf("Erro: Fila Cheia\n");
    }else{

        f->tamanho++;
        f->fila[f->tamanho] = x;
        
    }

}

//Desenfileirar item
int fila_Remove(TFila *f){

    int aux;

    if(fila_Vazia(f) == 1){
        printf("Erro: Fila Vazia\n");
        return -999;
    }else{
        
        aux = f->fila[0];

        for (int i = 0; i < f->tamanho; i++)
        {
          
          f->fila[i] = f->fila[i+1];
          printf("%i | ",f->fila[i]);
          
        }
        printf("\n");
        
        f->tamanho--;  
        return aux;
    }

}

int main()
{
    
    TFila *f;

    f = (TFila*)malloc(sizeof(TFila));

    fila_Inicia(f);
    fila_Insere(f,1);
    fila_Insere(f,2);
    fila_Insere(f,3);

    int aux;
    
    aux = fila_Remove(f);

    printf("Saiu: %i\n",aux);
    
    fila_Insere(f,53);

    free(f);

    return 0;
}
