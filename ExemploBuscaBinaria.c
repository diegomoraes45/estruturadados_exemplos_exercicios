#include <stdio.h>

int main()
{

	int vet[5] = {1,2,5,7,8};
	int valor, inicio, meio, fim, encontrou, tam;
        
    inicio = 0;
    fim = 4;
    encontrou = 0;

    printf("Digite o valor para busca:\n");
    scanf("%i",&valor);

    while(encontrou==0 && inicio<=fim){

        meio = (inicio+fim)/2;

        if(valor==vet[meio]){

           printf("O valor foi encontrado na posição %i\n",meio);
           encontrou = 1;

        }

        if(valor>vet[meio]){
            
            inicio = meio+1;
           

        }else{

        	fim = meio-1;
        }         

    }

    if(encontrou==0){
    	printf("O valor não foi encontrado\n");
    }	

	return 0;
        
}
