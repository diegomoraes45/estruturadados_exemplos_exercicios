#include <stdio.h>
#include <string.h>
#include <stdlib.h>


typedef struct{

   int idade;
   float altura;
   char nome[20];

}pessoa;



int main()
{
  
  pessoa *p;

  p=(pessoa *)malloc(sizeof(pessoa));

  printf("Digite o nome: \n");
  scanf("%s",p->nome);

  printf("Digite a idade: \n");
  scanf("%i",&p->idade);

  printf("Digite a altura: \n");
  scanf("%f",&p->altura);
  
  printf("Nome: %s\n",p->nome);
  printf("Idade: %i\n",p->idade);
  printf("Altura: %.2f\n",p->altura);


  free(p);
    

  return 0;
}