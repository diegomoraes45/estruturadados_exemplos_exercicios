#include <stdio.h>
#include <string.h>


typedef struct{

   int idade;
   float altura;
   char nome[20];

}pessoa;

int main()
{  

   pessoa p;
   pessoa *pp;

   pp=&p;

   /*strcpy((*pp).nome,"Fulano");
   (*pp).idade = 34;
   (*pp).altura = 1.76; */

    strcpy(pp->nome,"Fulano");
    pp->idade = 34;
    pp->altura = 1.76;

    
    printf("Nome: %s \n",p.nome);
    printf("Idade: %i\n",p.idade);
    printf("Peso: %.2f\n",p.altura); 
    
    /*printf("Nome: %s \n",(*pp).nome);
    printf("Idade: %i\n",(*pp).idade);
    printf("Peso: %.2f\n",(*pp).altura); */

   /* printf("Nome: %s \n",pp->nome);
    printf("Idade: %i\n",pp->idade);
    printf("Peso: %.2f\n",pp->altura); */
    
   /* printf("Nome: %p \n",&pp->nome);
    printf("Idade: %p\n",&pp->idade);
    printf("Peso: %p\n",&pp->altura); */
    
   // printf("Endereço da Estrutura: %p",pp);
   
   
  return 0;
}